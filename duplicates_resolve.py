#!/usr/bin/env python3

from repair import *
import json
import pandas as pd

# Find duplicated tickets (same alarms/nodes) with status "Assigned"
def get_duplicates(snow):
    #df = pd.DataFrame(snow.get_incidents(fields=snow.FULL_FIELDS, status='Assigned'))
    df = pd.DataFrame(snow.get_incidents(fields=snow.FULL_FIELDS, status='NotResolved'))

    # duplicate = same CI + same alarm
    dups = df[df.duplicated(['cmdb_ci', 'u_alarms_list'], keep=False)]
    logging.info(f"Found {len(dups)} duplicates")
    return dups

"""
Find duplicate tickets (same CI and alarm), keep the oldest open, attach and close next ones
"""
if __name__ == '__main__':
    dry_run = 0

    r = RepairSnow('config.toml')
    r.auth()

    # For each duplicate group
    for name, group in get_duplicates(r).groupby(['cmdb_ci', 'u_alarms_list']):
        logging.info(f"Parsing duplicates for {name[0]} + {name[1]}")

        # Sort by number (like "creation date" but without type conversion)
        my_duplicates = group.sort_values('number')
        oldest = my_duplicates.iloc[:1, :]
        remaining = my_duplicates.iloc[1:, :]

        for idx, row in remaining.iterrows():
            payload = {
                'assigned_to': r.config['snow']['fppsnow'],
                'incident_state': '6', # Resolved
                'comments': r.config['duplicates_resolve']['close_comment'],
                'parent': oldest.iloc[0]['sys_id'],
                'u_close_code': 'Other',
                'u_no_notification': 'True',
                'u_exclude': 'True',
            }
            if dry_run:
                logging.info(f"DRY RUN: Closing {row['number']} as duplicate of {oldest.iloc[0]['number']}")
                logging.info(json.dumps(payload, indent=2))
                continue

            logging.info(f"Closing {row['number']} as duplicate of {oldest.iloc[0]['number']}")
            try:
                r.get_resource('/table/incident').update(query={'number': row['number']}, payload=payload)
            except Exception as e:
                print(e)
