#!/usr/bin/env python3
import requests, urllib3, logging
import pandas as pd

class DCIMQuery:
    DCS = ['0513', '0613', '0773', '0775', '0874', '6045', '6074']

    def __init__(self, api_url, api_username, api_key):
        self.ODC_API_URL = api_url
        self.ODC_USERNAME = api_username
        self.ODC_APIKEY = api_key

        logging.basicConfig(level = logging.INFO, format = '%(funcName)s[%(process)d] %(asctime)s %(levelname)s:  %(message)s')
        urllib3.disable_warnings()

    def fetchAPI(self, url):
        logging.debug('Requesting {}'.format(url))
        headers = {
            'UserID': self.ODC_USERNAME,
            'APIKey': self.ODC_APIKEY,
        }
        req = requests.get(self.ODC_API_URL + url, headers = headers, verify=False)
        req.raise_for_status()

        return req.json()

    def findCabinet(self, s):
        res = self.fetchAPI('/cabinet?wildcards&Attributes=CabinetID,Location,CabRowID,DataCenterID,DataCenterName,ZoneID&Location={}'.format(s))
        #return res['cabinet']
        return list(filter(lambda c: c['DataCenterName'].startswith(tuple(self.DCS)), res['cabinet']))

    def getDevicesInCabinet(self, cabinet_id):
        res = self.fetchAPI('/cabinet/{}/devices?Attributes=DeviceID,DeviceType,Model,Label,SerialNo,PrimaryIP,Status,RearChassisSlots'.format(cabinet_id))
        return res['device']

    def getDevice(self, device):
        res = self.fetchAPI('/device?wildcards&Status=Production&Label={}'.format(device))
        if len(res['device']) == 0:
            res = self.fetchAPI('/device?wildcards&Status=Production&SerialNo={}'.format(device))
        if len(res['device']) != 1:
            logging.warning(f'Multiple (or no) matches found for {device}')
            return None
        return res['device'][0]

    # Returns a dict of values
    def fetchPDUMetrics(self, p):
        try:
            res = self.fetchAPI('/pdustats/{}'.format(p['DeviceID']))['pdustats']
            return {'LastRead': res['LastRead'], 'Label': p['Label'], 'W': int(res['Wattage']), 'L1': int(res['WattageL1']), 'L2': int(res['WattageL2']), 'L3': int(res['WattageL3'])}
        except:
            logging.warning('Unable to fetch metrics for PDU {}'.format(p['Label']))

    # Gets devices in a rack, keeps PDUs in 'production' only
    def getActivePDUs(self, rack):
        # Much easier to filter data with pandas
        df = pd.DataFrame(self.getDevicesInCabinet(rack['CabinetID']))
        try:
            pdus = df[(df.DeviceType == 'CDU') & (df.Status == 'Production')]
            return pdus.to_dict('records')
        except AttributeError:
            logging.debug('No PDU found in rack {}'.format(rack))
