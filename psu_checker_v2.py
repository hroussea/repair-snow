#!/usr/bin/env python3
import os
import sys
import logging
import subprocess
import argparse
import re
from typing import Dict, List, Optional, Tuple

import pandas as pd
import multiprocessing as mp
from tqdm import tqdm

from repair import *
from opendcim import DCIMQuery
from pyipmidb.client import IPMIDBClient, IPMIDBError

# Configure logging
logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s - %(levelname)s - %(message)s'
)
logger = logging.getLogger(__name__)

class ServerPSUChecker:
    def __init__(
        self,
        api_url: str,
        api_username: str,
        api_key: str,
        show_progress: bool = False
    ):
        """
        Initialize the ServerPSUChecker with API credentials

        :param api_url: URL for the DCIM API
        :param api_username: Username for API authentication
        :param api_key: API key for authentication
        :param show_progress: Whether to show progress bar
        """
        if not all([api_url, api_username, api_key]):
            raise ValueError('API details not properly set')

        self.api_url = api_url
        self.api_username = api_username
        self.api_key = api_key
        self.show_progress = show_progress

        # Initialize DCIM query client
        self.dcim = DCIMQuery(self.api_url, self.api_username, self.api_key)

        # Initialize IPMI client
        self.ipmidb = IPMIDBClient()

    def find_servers(self, rack_pattern: str) -> pd.DataFrame:
        """
        Find servers in specified rack(s)

        :param rack_pattern: Rack pattern to search
        :return: DataFrame of servers
        """
        # Find matching racks
        racks_found = [i['CabinetID'] for i in self.dcim.findCabinet(rack_pattern) if i is not None]

        logger.info(f'Found {len(racks_found)} racks matching pattern: {rack_pattern}')
        logger.info(f'Fetching devices details for {len(racks_found)} racks...')

        # Multiprocessing to fetch devices
        with mp.Pool(processes=mp.cpu_count()) as pool:
            devices_list = []
            for d in (tqdm(pool.imap_unordered(self.dcim.getDevicesInCabinet, racks_found),
                           total=len(racks_found), disable=not self.show_progress) if self.show_progress
                     else pool.imap_unordered(self.dcim.getDevicesInCabinet, racks_found)):
                if d is not None:
                    devices_list.extend(d)

        logger.info(f'{len(devices_list)} devices identified')
        df = pd.DataFrame(devices_list)

        if len(df) == 0:
            logger.error(f'No servers could be found for rack {rack_pattern}')
            sys.exit(1)

        # Filter for production servers
        servers = df.loc[((df['DeviceType'] == 'Server') | (df['DeviceType'] == 'Chassis')) & (df['Status'] == 'Production') & (df['RearChassisSlots'] == 0)]

        logger.info(f'{len(servers)} production servers found')
        return servers

    def get_psu_state(self, serial: str) -> Tuple[str, bool, Optional[str]]:
        """
        Check PSU state for a given server

        :param serial: Server serial number
        :return: Tuple of (serial, is_psu_ok, output)
        """
        try:
            # Fetch IPMI credentials
            creds = self.ipmidb.query_ipmidb(f'node?serial={serial}')

            if 'bmcip' not in creds:
                logger.warning(f'No credentials found for {serial}')
                return (serial, False, None)

            # Run IPMI command to check PSU status
            cmd = (f"ipmiproxy -H {creds['bmcip']} -U {creds['username']} "
                   f"-P {creds['password']} sdr type 0x08 | grep -i ps.*status")

            rc = subprocess.check_output(
                cmd,
                shell=True,
                stderr=subprocess.STDOUT,
                universal_newlines=True,
                timeout=30
            )

            # Check if all PSUs are OK
            ok = all(
                re.match(r'^.*ok.*Presence detected$', line, flags=re.IGNORECASE)
                for line in rc.splitlines()
            )

            return (serial, ok, rc if not ok else '')

        except Exception as e:
            logger.error(f'Error checking PSU for {serial}: {e}')
            return (serial, False, str(e))

    def check_servers_psu(self, rack_pattern: str) -> pd.DataFrame:
        """
        Check PSU status for servers in specified rack(s)

        :param rack_pattern: Rack pattern to search
        :return: DataFrame of servers with PSU issues
        """
        # Find servers
        servers = self.find_servers(rack_pattern)

        # Check PSU status with multiprocessing
        with mp.Pool(processes=mp.cpu_count()) as pool:
            psu_results = list(
                tqdm(
                    pool.imap_unordered(self.get_psu_state, servers['SerialNo']),
                    total=len(servers),
                    disable=not self.show_progress
                ) if self.show_progress
                else pool.imap_unordered(self.get_psu_state, servers['SerialNo'])
            )

        # Create results DataFrame
        tmp = [
            {'serial': serial, 'result': res, 'output': output}
            for serial, res, output in psu_results
        ]
        result_df = pd.DataFrame(tmp)
        result_df = result_df.loc[result_df['result'] != True]

        logger.info(f'{len(result_df)} servers with bad PSU status')
        return result_df

def main():
    # Parse command-line arguments
    parser = argparse.ArgumentParser(description='Check server PSU status')
    parser.add_argument('rack_pattern', help='Rack pattern to search')
    parser.add_argument('--progress', action='store_true', help='Show progress bar')
    parser.add_argument('--log-level', default='INFO',
                        choices=['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'],
                        help='Set logging level')
    args = parser.parse_args()

    # Set logging level
    logging.getLogger().setLevel(getattr(logging, args.log_level))

    # Initialize and run PSU checker
    try:
        checker = ServerPSUChecker(
            API_URL,
            API_USERNAME,
            API_KEY,
            show_progress=args.progress
        )

        broken_servers = checker.check_servers_psu(args.rack_pattern)

        if len(broken_servers) > 0:
            logger.warning(f'Found {len(broken_servers)} servers with bad PSU status')
            pd.set_option('display.max_colwidth', None)
            print(broken_servers)
            sys.exit(-1)
        else:
            logger.info('No servers found with bad PSU status')
            sys.exit(0)

    except Exception as e:
        logger.error(f'Unexpected error: {e}')
        sys.exit(1)

if __name__ == '__main__':

    # Get API related data from config
    r = RepairSnow('config.toml')

    API_URL = r.config['opendcim']['endpoint']
    API_USERNAME= r.config['opendcim']['username']
    API_KEY = r.config['opendcim']['apikey']

    main()
