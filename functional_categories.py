#!/usr/bin/env python3
# vim: ai ts=2 sts=2 et sw=2 ft=python

from repair import *
from opendcim import DCIMQuery

import sys
import json
import pandas as pd
from datetime import datetime
from prettytable import PrettyTable

dry_run = False
rc = 0

def pick_warranty(node):
    today = datetime.today().date()
    try:
        warranty_end = datetime.strptime(node['WarrantyExpire'], '%Y-%m-%d').date()
        retirement_end = datetime.strptime(node['RetirementEnd'], '%Y-%m-%d').date()
    except ValueError as e:
        logging.warning(f"Unable to parse dates: WarrantyExpire: {node['WarrantyExpire']}, RetirementEnd: {node['RetirementEnd']}")
        return None

    # Is "Extended" the active warranty
    is_extended = "EXTENDED" in node['WarrantyCo']
    # Is Warranty active ?
    is_warranty = warranty_end >= today
    # Is Limited ?
    is_limited = retirement_end >= today

    # TODO: Safety check: Warranty shall be extended for best effort to be active

    # If WarrantyEnd > today and not extended => Full support
    if is_warranty and not is_extended and is_limited:
        functional_category = 'full_support'
        logging.debug(f"{node['SerialNo']} FULL")
    # If WarrantyEnd > today and extended => Limited support
    elif not is_warranty and is_limited:
        functional_category = 'limited_support'
        logging.debug(f"{node['SerialNo']} LIMITED")
    # Explicit extended warranty
    elif is_extended and is_warranty and is_limited:
        functional_category = 'limited_support'
        logging.debug(f"{node['SerialNo']} LIMITED v2")
    # If WarrantyEnd < today + RetirementEnd < today
    elif not is_warranty and not is_limited:
        functional_category = 'besteffort_support'
        logging.debug(f"{node['SerialNo']} BestEffort")
    else:
        functional_category = None
        logging.warning(f"{node['SerialNo']} UNDEFINED: MfgDate: {node['MfgDate']} WarCo: {node['WarrantyCo']} WarEnd: {node['WarrantyExpire']} RetEnd: {node['RetirementEnd']} (is_war?{is_warranty} is_limited:{is_limited}")
    return functional_category

def update_functional_category(ticket, category):
    if dry_run:
        logging.info(f"DRY RUN: Updating functional category for {ticket} to {category}")
        logging.info(json.dumps(payload, indent=2))
        return

    logging.info(f"{ticket}: Updating functional category to {category}")

    try:
        inc_table = r.get_resource('/table/incident')
        #old_inc = inc_table.get(query={'number': ticket})

        # Change functional category
        payload = { 'u_functional_category': r.config['functional_categories'][category], }
        res = inc_table.update(query={'number': ticket}, payload=payload)
    except Exception as e:
        print(e)

"""
Fetches all assigned incidents with no functional category, look up their warranty information and update
the functional category accordingly
"""
if __name__ == '__main__':

    r = RepairSnow('config.toml')
    r.auth()

    dcim = DCIMQuery(r.config['opendcim']['endpoint'], r.config['opendcim']['username'], r.config['opendcim']['apikey'])

    # Get all assigned tickets
    df = pd.DataFrame(r.get_incidents(fields=r.FULL_FIELDS, status='Assigned'))
    #df = pd.DataFrame(r.get_incidents(fields=r.FULL_FIELDS, status='NotResolved'))
    cis = df.loc[df['u_functional_category'] == ''].groupby("cmdb_ci")
    #cis = df.groupby("cmdb_ci")
    
    logging.info(f'Found {len(cis)} elements')
    for ci, ci_group in cis:
        logging.info(f'Getting warranty data for {ci}')
        # Fetching warranty details for ci from OpenDCIM
        logging.debug(f'Fetching {ci} details from OpenDCIM')
        try:
            dcim_data = dcim.getDevice(ci)
        except Exception as e:
            logging.warning(f'Unable to get {ci} data from OpenDCIM: {e}')
            continue
        if dcim_data is None:
            # No/multiple matches found, log printed by library, just continue
            continue
        category = pick_warranty(dcim_data)
        if category is None:
            #rc = 1 # Flag run as failed
            continue

		# Now that we have CI warranty status, iterate over incidents for this CI
        for i, inc in ci_group.iterrows(): 
            update_functional_category(inc['number'], category)

    sys.exit(rc)
