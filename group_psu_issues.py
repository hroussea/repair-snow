#!/usr/bin/env python3
# vim: ai ts=2 sts=2 et sw=2 ft=python

from repair import *
import json
import pandas as pd
import re

# Find duplicated tickets (same alarms/nodes) with status "Assigned"
def get_duplicates(snow):
    df = pd.DataFrame(snow.get_incidents(fields=snow.FULL_FIELDS, status='Assigned'))

    # duplicate = same alarm
    dups = df[df.duplicated(['u_alarms_list'], keep=False)]
    logging.info(f"Investigating {len(dups)} tickets")
    return dups

"""
Find tickets with same alarm and chassis, keep the oldest open, attach and close next ones
"""
if __name__ == '__main__':
    dry_run = 1

    r = RepairSnow('config.toml')
    r.auth()

    # For each duplicate group
    for alarm_name, alarms_list in get_duplicates(r).groupby(['u_alarms_list']):
        logging.debug(f"Parsing alarms for {alarm_name}...")

        if alarm_name != 'cinnamon_sdr_power supply_state_asserted':
            logging.debug(f"Ignoring {alarm_name}")
            continue

        # From here, only relevant tickets are treated :)
        tickets_per_quad = {}

        # Sort by number (like "creation date" but without type conversion)
        sorted_alarms = alarms_list.sort_values('number')
        logging.info(f'Found {len(sorted_alarms)} cinnamon_sdr_power supply_state_asserted tickets')
        # TODO: tickets are now created against the hostname and not the serial number anymore
        quads = sorted_alarms['cmdb_ci'].str.extract('^(.*)-(0?[1-4]|[A-D])$')
        for quad in quads[0]:
            if pd.isnull(quad):
                # Not a quad...
                continue
            incs_idx = sorted_alarms['cmdb_ci'].str.match(quad)
            incs = sorted_alarms[incs_idx]
            if len(incs) <= 1:
                # Not grouping with only one incident
                continue
            logging.info(f'Found {len(incs)} tickets for {quad}')
            oldest = incs.iloc[:1, :]
            remaining = incs.iloc[1:, :]

            for idx, row in remaining.iterrows():
                payload = {
                                        'assigned_to': r.config['snow']['fppsnow'],
                                        'incident_state': '6', # Resolved
                                        'comments': r.config['group_psu_issues']['close_comment'],
                                        'parent': oldest.iloc[0]['sys_id'],
                                        'u_close_code': 'Other',
                                        'u_no_notification': 'True',
                                        'u_exclude': 'True',
                                }
                if dry_run:
                    logging.info(f"DRY RUN: Closing {row['number']} as duplicate of {oldest.iloc[0]['number']}")
                    logging.info(json.dumps(payload, indent=2))
                    continue

                logging.info(f"Closing {row['number']} as duplicate of {oldest.iloc[0]['number']}")
                try:
                    r.get_resource('/table/incident').update(query={'number': row['number']}, payload=payload)
                except Exception as e:
                    logging.error(e)

