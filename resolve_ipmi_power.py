#!/usr/bin/env python3

from repair import *
import json
import pandas as pd
import re

# Find duplicated tickets (same alarms/nodes) with status "Assigned"
def get_duplicates(snow):
    df = pd.DataFrame(snow.get_incidents(fields=snow.FULL_FIELDS, status='Assigned'))

    # duplicate = same alarm
    dups = df[df.duplicated(['u_alarms_list'], keep=False)]
    logging.info(f"Investigating {len(dups)} tickets")
    return dups

def get_psus_state(serial):
    import subprocess
    from pyipmidb.client import IPMIDBClient, IPMIDBError
    ipmidb = IPMIDBClient()
    try:
        creds = ipmidb.query_ipmidb(f'node?serial={serial}')
        if 'bmcip' not in creds:
            logging.warning(f'No credentials found for {serial}')
    except Exception as e:
        logging.error(f'Error while fetching credentials: {e}')

    # Type 0x08 is 'Power Supplies'
    logging.debug(f"Querying {serial} with following parameters: {creds}")
    try:
        rc = subprocess.check_output(f"ipmiproxy -H {creds['bmcip']} -U {creds['username']} -P {creds['password']} sdr type 0x08 | grep -i ps.*status", shell=True, stderr=subprocess.STDOUT, universal_newlines=True, timeout=30)
    except Exception as e:
        logging.error(f'Unable to get IPMI result: {e}')
        return (False, None)

    # Only keep lines not containing 'ok.*Presence detected$'
    ok = True
    for l in rc.splitlines():
      if not re.match(r'^.*ok.*Presence detected$', l, flags=re.IGNORECASE|re.MULTILINE):
        ok = False
    return (ok, rc)

"""
Find ipmi_power alarms, check current PSUs status, solve ticket if both PSUs are OK
"""
if __name__ == '__main__':
    dry_run = 0

    r = RepairSnow('config.toml')
    r.auth()

    # For each duplicate group
    for alarm_name, alarms_list in get_duplicates(r).groupby(['u_alarms_list']):
        logging.debug(f"Parsing alarms for {alarm_name}...")

        if alarm_name != 'ipmi_power':
            logging.debug(f"Ignoring {alarm_name}")
            continue

        # From here, only relevant tickets are treated :)
        tickets_per_quad = {}

        # Sort by number (like "creation date" but without type conversion)
        sorted_alarms = alarms_list.sort_values('number')
        logging.info(f'Found {len(sorted_alarms)} ipmi_power tickets')
        for i, node in sorted_alarms.iterrows():
            serial = node['cmdb_ci'] # Assume ticket is created by Cinnamon which sets this field properly
            ticket = node['number']
            logging.debug(f"Working on node: {serial}")
            status_ok, output = get_psus_state(serial)
            if not status_ok:
              	# Not touching ticket
              	logging.info(f'Ticket {ticket} for {serial}: Real PSU failure detected:\n{output}')
            else:
                payload = {
                   'assigned_to': r.config['snow']['fppsnow'],
                   'incident_state': '6', # Resolved
                   'comments': r.config['resolve_ipmi_power']['close_comment'] + '\nAll PSUs are working properly: \n' + output,
                   #'parent': oldest.iloc[0]['sys_id'],
                   'u_close_code': 'Duplicated',
                   'u_no_notification': 'True',
                   'u_exclude': 'True',
                }
                if dry_run:
                    logging.info(f"DRY RUN: Closing {ticket}, PSUs reported state: OK")
                    logging.info(json.dumps(payload, indent=2))
                    continue

                logging.info(f"Closing {ticket}, PSUs checked: OK")
                try:
                    r.get_resource('/table/incident').update(query={'number': ticket}, payload=payload)
                except Exception as e:
                    logging.error(e)
