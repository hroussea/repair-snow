#!/usr/bin/env python3

from repair import *
import json, sys
import pandas as pd

import smtplib
from email.message import EmailMessage

message = """
(Message generated automatically)

Dear Repair team,
Apparently you are more than two people taking care of the same faulty equipment (maybe with different issues).
It is not a problem as soon as it is under control and coordinated between you.
Thank you.

"""
def get_duplicate_assignees(snow):
    df = pd.DataFrame(snow.get_incidents(fields=['number', 'u_alarms_list', 'cmdb_ci', 'assigned_to']))
    
    logging.info("Keeping only duplicates...")
    # Multiple tickets for unique CI
    dups = df[df.duplicated(['cmdb_ci'], keep=False)]
    # Multiple "assigned_to for unique CI
    real_dups = dups[dups.groupby('cmdb_ci')['assigned_to'].transform('nunique') > 1]

    return real_dups 

if __name__ == '__main__':

    r = RepairSnow('config.toml')
    r.auth()

    not_alone = get_duplicate_assignees(r).sort_values(by=['cmdb_ci'])
    if len(not_alone) == 0:
        logging.info("No tickets with multiple workers found...")
        sys.exit(0)

    for i, row in not_alone.iterrows():
        message += f"{row['cmdb_ci']} / {row['number']} / {row['u_alarms_list']} / {row['assigned_to']}\n"

    logging.info("Sending e-mail...")
    msg = EmailMessage()
    msg.set_content(message)
    msg['Subject'] = '[Repair service] - duplicate InHand tickets'
    msg['From'] = 'noreply@cern.ch'
    msg['To'] = ', '.join(r.config['am_i_alone']['recipients'])

    s = smtplib.SMTP('localhost')
    s.send_message(msg)
    s.quit()
