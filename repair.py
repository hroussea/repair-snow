#!/usr/bin/env python3

import pysnow
import requests, logging, tomli

class RepairSnow:
    # 'Assignment group' is 'Itprocos vendor 2nd Line support'
    SNOW_REPAIR_GROUP = ''
    DEFAULT_FIELDS = []
    FULL_FIELDS = []

    def __init__(self, config_file):
        logging.getLogger().setLevel(logging.INFO)
        self.config = self._read_config(config_file)

        self.DEFAULT_FIELDS = self.config['snow']['default_fields']
        self.FULL_FIELDS = self.config['snow']['full_fields']
        self.SNOW_REPAIR_GROUP= self.config['snow']['repair_group']

    def _read_config(self, filename):
        with open(filename, "rb") as f:
            config = tomli.load(f)
        return config

    def auth(self):
        logging.debug("Preparing session Basic Auth session")
        self.session = requests.Session()
        self.session.auth = requests.auth.HTTPBasicAuth(self.config['snow']['username'], self.config['snow']['password'])
        logging.debug("Instantiating SNOW REST client")
        self.client = pysnow.Client(instance=self.config['snow']['instance'], session=self.session)
        self.client.parameters.display_value = True
        self.client.parameters.exclude_reference_link = True

    def get_incidents(self, fields=DEFAULT_FIELDS, status=None, ci=None):
        logging.info("Fetching all incidents")

        qb = (pysnow.QueryBuilder()
              .field('active').equals('true')
              .AND()
              .field('assignment_group').equals(self.SNOW_REPAIR_GROUP)
              .AND().field('cmdb_ci.name').is_not_empty()
        )

        # Append status-dependent condition
        if status == 'NotResolved':
            qb = qb.AND().field('incident_state').not_equals('6')
        elif status == 'Resolved':
            qb = qb.AND().field('incident_state').equals('6')
        elif status == 'Assigned':
            qb = qb.AND().field('incident_state').equals('2')
        else:
            logging.debug("No status provided, defaulting to not 'Assigned' + NotResolved => InHand")
            qb = qb.AND().field('incident_state').not_equals('2').AND().field('incident_state').not_equals('6')

        # Only matching a specific Configuration Item
        if ci is not None:
            qb.AND().field('cmdb_ci.name').equals(ci)
        
        inc_table = self.client.resource(api_path='/table/incident')
        res = inc_table.get(query=qb, fields=fields, stream=True)
        return list(res.all())

    def get_incident(self, number):
        logging.debug("Fetching incident %s" % number)
        inc_table = self.client.resource(api_path='/table/incident')
        res = inc_table.get(query={'number': number})
        return res.one()

    def get_resource(self, resource_path):
        logging.debug("Returning incident resource")
        return self.client.resource(api_path=resource_path)
